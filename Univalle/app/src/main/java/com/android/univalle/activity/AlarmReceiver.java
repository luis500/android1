package com.android.univalle.activity;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v7.app.NotificationCompat;

import com.android.univalle.db.OpenHelper;


/**
 * Created by ptyagi on 4/17/17.
 */

/**
 * AlarmReceiver handles the broadcast message and generates Notification
 */
public class AlarmReceiver extends BroadcastReceiver {
    public static String NOTIFICATION_ID = "notification_id";

    @Override
    public void onReceive(Context context, Intent intent) {
        //Get notification manager to manage/send notifications


        //Intent to invoke app when click on notification.
        //In this sample, we want to start/launch this sample app when user clicks on notification
        String title = intent.getStringExtra(OpenHelper.COLUMN_TITLE);
        Intent intentToRepeat = new Intent(context, NoteActivity.class);

        intentToRepeat.putExtra(OpenHelper.COLUMN_TITLE, title);
        intentToRepeat.putExtra(OpenHelper.COLUMN_TYPE, intent.getIntExtra(OpenHelper.COLUMN_TYPE, 0));
        intentToRepeat.putExtra("position", intent.getIntExtra("position", 0));
        intentToRepeat.putExtra(OpenHelper.COLUMN_ID, intent.getLongExtra(OpenHelper.COLUMN_ID, 0));
        intentToRepeat.putExtra(OpenHelper.COLUMN_PARENT_ID, intent.getLongExtra(OpenHelper.COLUMN_PARENT_ID, 0));
        intentToRepeat.putExtra(OpenHelper.COLUMN_THEME, intent.getIntExtra(OpenHelper.COLUMN_THEME, 0));

        //set flag to restart/relaunch the app
        intentToRepeat.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        //Pending intent to handle launch of Activity in intent above
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context, NotificationHelper.ALARM_TYPE_RTC, intentToRepeat, PendingIntent.FLAG_UPDATE_CURRENT);

        //Build notification
        Notification repeatedNotification = buildLocalNotification(context, pendingIntent, title).build();

        //Send local notification
        NotificationHelper.getNotificationManager(context).notify(NotificationHelper.ALARM_TYPE_RTC, repeatedNotification);
    }

    public NotificationCompat.Builder buildLocalNotification(Context context, PendingIntent pendingIntent, String title) {
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(android.R.drawable.arrow_up_float)
                        .setContentTitle("Ha llegado la hora limite de: " + title)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                        .setAutoCancel(true);

        return builder;
    }
}