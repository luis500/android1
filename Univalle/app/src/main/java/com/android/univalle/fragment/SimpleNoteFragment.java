package com.android.univalle.fragment;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.android.univalle.R;
import com.android.univalle.fragment.template.NoteFragment;
import com.android.univalle.model.DatabaseModel;

import java.util.Calendar;

import jp.wasabeef.richeditor.RichEditor;

public class SimpleNoteFragment extends NoteFragment implements View.OnClickListener {
	private RichEditor body;


	//Dates
	Button buttonDate;
	Button buttonTime;
	EditText editTextDate;
	EditText editTextTime;

	public SimpleNoteFragment() {}

	@Override
	public int getLayout() {
		return R.layout.fragment_simple_note;
	}

	@Override
	public void saveNote(final SaveListener listener) {
		super.saveNote(listener);
		note.body = body.getHtml();
		note.date = editTextDate.getText().toString();
		note.time = editTextTime.getText().toString();

		new Thread() {
			@Override
			public void run() {
				long id = note.save();
				if (note.id == DatabaseModel.NEW_MODEL_ID) {
					note.id = id;
				}
				listener.onSave();
				interrupt();
			}
		}.start();
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		buttonDate = (Button) view.findViewById(R.id.buttonDate);
		buttonTime = (Button) view.findViewById(R.id.buttonTime);
		editTextDate = (EditText) view.findViewById(R.id.editTextDate);
		editTextTime = (EditText) view.findViewById(R.id.editTextTime);

		buttonDate.setOnClickListener(this);
		buttonTime.setOnClickListener(this);
		editTextDate.setText(note.date);
		editTextTime.setText(note.time);
	}

	@Override
	public void init(View view) {
		body = (RichEditor) view.findViewById(R.id.editor);
		body.setPlaceholder("Note");
		body.setEditorBackgroundColor(ContextCompat.getColor(getContext(), R.color.bg));

		//view.findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
			//@Override public void onClick(View v) {
				//body.setBold();
			//}
		//});

		//view.findViewById(R.id.action_italic).setOnClickListener(new View.OnClickListener() {
			//@Override public void onClick(View v) {
			//	body.setItalic();
			//}
		//})

		//view.findViewById(R.id.action_underline).setOnClickListener(new View.OnClickListener() {
			//@Override public void onClick(View v) {
			//	body.setUnderline();
			//}
		//});

		body.setHtml(note.body);
	}

	@Override
	public void onClick(View view) {
		if (view == buttonDate) {
			final Calendar calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.getTime();
			DatePickerDialog datePickerDialog = new DatePickerDialog(this.getActivity(), new DatePickerDialog.OnDateSetListener() {
				@Override
				public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
					editTextDate.setText(i2 + "/" + (i1 + 1) + "/" + i);
					if (editTextTime.getText().toString().trim().isEmpty()) {
						editTextTime.setText(calendar.get(Calendar.HOUR_OF_DAY)+ ":" + calendar.get(Calendar.MINUTE));
					}
				}
			}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

			datePickerDialog.show();
		}
		if (view == buttonTime) {
			final Calendar calendar = Calendar.getInstance();
			TimePickerDialog timePickerDialog = new TimePickerDialog(this.getActivity(), new TimePickerDialog.OnTimeSetListener() {
				@Override
				public void onTimeSet(TimePicker timePicker, int i, int i1) {
					editTextTime.setText(i + ":" + (i1 < 10 ? "0" + i1 : i1));
				}
			}, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true);
			timePickerDialog.show();
		}
	}
}
